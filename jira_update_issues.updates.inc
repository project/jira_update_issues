<?php
/**
 * Created by PhpStorm.
 * User: michaelpporter
 * Date: 5/26/16
 * Time: 1:08 PM
 */

/**
 * Send isses to JIRA.
 */
function jira_update_issues_send_to_jira() {
  // Running Drupal's system updates.
  update_refresh();
  update_fetch_data();

  $now = time();
  variable_set('jira_update_issues_last_check', $now);

  if ($available = update_get_available(TRUE)) {
    module_load_include('inc', 'update', 'update.compare');
    $data = update_calculate_project_data($available);

    $project_id = variable_get('jira_update_issues_project_id', 0);
    $project_code = variable_get('jira_update_issues_project_code', '');

    $count = 0;
    foreach ($data as $key => $value) {
      if (isset($value['info']['version']) && !empty($value['info']['version'])
        && isset($value['recommended']) && !empty($value['recommended'])
        && $value['info']['version'] <> $value['recommended']
      ) {
        $security_level = variable_get('jira_update_issues_level', 4);
        if ($value['status'] == 1
          || ($value['status'] == 4
            && $security_level == 4)
        ) {
          $summary = 'Update ' . $value['name'] . ' to ' . $value['recommended'];
          $description = $summary . ' https://www.drupal.org/project/' . $value['name'];
          $issue_type_id = 2;
          if ($value['status'] == 1) {
            $summary = 'SECURITY ' . $summary;
            $issue_type_id = 1;
          }
          if ($value['status'] == 4) {
            $summary = 'RELEASE ' . $summary;
            $issue_type_id = 2;
          }

          $found_issues = jira_rest_issue_search('summary ~  "' . $summary .
            '" AND project = ' . $project_code . ' ORDER BY updatedDate DESC');
          if (!count($found_issues)) {

            // Append create description.
            $create_description = $description;
            if (variable_get('jira_update_issues_description_append_create')) {
              $create_description = $description . "\n\n" . variable_get('jira_update_issues_description_append_create');
            }

            $issue_data = array(
              'fields' => array(
                'project'     => array('id' => $project_id),
                'summary'     => $summary,
                'description' => $create_description,
                'priority'    => array('id' => (string) $value['status']),
                'issuetype'   => array('id' => (string) $issue_type_id),
                'labels'      => array($value['name']),
                'assignee'    => array('name' => (string) variable_get('jira_update_issues_assignee', '')),
              ),
            );
            $jira = jira_rest_issue_create($issue_data);

            // Update the issue if it was checked to run on security update.
            if ($issue_type_id == 1 && variable_get('jira_update_issues_update_on_security')) {
              // Append update description.
              $update_description = $description;
              if (variable_get('jira_update_issues_description_append_update')) {
                $update_description = $description . "\n\n" . variable_get('jira_update_issues_description_append_update');
              }

              $issue_data['fields']['description'] = $update_description;

              jira_rest_issue_update($jira->key, $issue_data);
            }
          }
          $count = $count + 1;
        }
      }
    }

  }

  return '';
}
